import Layout from '../../components/layout';
import Head from 'next/head';
import {getAllPostIds,getPostData} from '../../lib/posts.js';
import utilsStyles from '../../styles/utils.module.css'
import DateParse from '../../components/DateParse';

export default function Post({postData}) {
  return <Layout>
<Head>
<title> {postData.title}</title>
</Head>
<article>
        <h1 className={utilsStyles.headingXl}>{postData.title}</h1>
        <div className={utilsStyles.lightText}>
         {postData.date} 
        </div>
        <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
      </article>
  </Layout>
}
export async function getStaticPaths() {
    const paths = getAllPostIds()
    return {
    paths,
    fallback: false
  }
  }
  
  

  export async function getStaticProps({ params }) {
   
    const postData =  await getPostData(params.id)
    return {
      props: {
        postData
      }
    }
  }