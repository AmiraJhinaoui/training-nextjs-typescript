
import styles from './alert.module.css';
import cn from 'classnames';


export function Alert ({children,type}:any) {
    return(
        <div
      className={cn({
        [styles.success]: type === 'success',
        [styles.error]: type === 'error'
      })}
    >
      {children}
    </div>
    )
}